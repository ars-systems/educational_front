import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { NotificationsComponent } from './components/user/notifications/notifications.component';
import { MessagesComponent } from './components/user/messages/messages.component';
import { MaterialComponent } from './components/user/material/material.component';
import { MyMaterialComponent } from './components/user/material/my-material/my-material.component';
import { SavedMaterialComponent } from './components/user/material/saved-material/saved-material.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { CalendarComponent } from './components/user/calendar/calendar.component';
import { ExitSettingsGuard } from './components/user/settings/exit.settings.guard';
import { PreferableComponent } from './components/user/preferable/preferable.component';
import { GroupsComponent } from './components/user/groups/groups.component';
import { MyGroupsComponent } from './components/user/groups/my-groups/my-groups.component';
import { SavedGroupsComponent } from './components/user/groups/saved-groups/saved-groups.component';
import { UpcomingLiveComponent } from './components/user/courses/upcoming-live/upcoming-live.component';
import { CoursesComponent } from './components/user/courses/courses.component';
import { MyLiveComponent } from './components/user/courses/my-live/my-live.component';
import { ParticipatedLiveComponent } from './components/user/courses/participated-live/participated-live.component';
import { ChatComponent } from './components/user/messages/chat/chat.component';
import { TestComponent } from './components/user/test/test.component';
import { AddTestComponent } from './components/user/test/add-test/add-test.component';
import { SavedTestComponent } from './components/user/test/saved-test/saved-test.component';
import { MyTestComponent } from './components/user/test/my-test/my-test.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "home",
    component: HomeComponent,
    data: {
      title:"Գլխավոր էջ"
    }
  },
  {
    path: "user",
    component: UserComponent,
    data: {
      title:"Իմ էջը"
    },
    children: [
      {
        path: '',
        redirectTo: 'notification',
        pathMatch:'full',
      },
      {
        path: "notification",
        component: NotificationsComponent,
        data: {
          title:"Նորություններ"
        }
      },
      {
        path: "message",
        component: MessagesComponent,
        data: {
          title:"Նամակներ"
        },
        children: [
          {
            path:"",
            redirectTo:"karen",
            pathMatch:"full",
          },
          {
            path:":id",
            component: ChatComponent,
            data: {
              title:"Կարեն"
            }
          }
        ]
      },
      {
        path: "material",
        component: MaterialComponent,
        data: {
          title:"Նյութեր"
        },
        children: [
          {
            path: "",
            redirectTo: 'my',
            pathMatch: 'full',
          },
          {
            path: "my",
            component: MyMaterialComponent,
            data: {
              title:"Իմ նյութերը"
            }
          },
          {
            path: "saved",
            component: SavedMaterialComponent,
            data: {
              title:"Իմ պահած նյութերը"
            }
          },
        ]
      },
      {
        path: "courses",
        component: CoursesComponent,
        data: {
          title:"Դասընթացներ"
        },
        children: [
          {
            path: "",
            redirectTo: 'upcoming',
            pathMatch: 'full',
          },
          {
            path: "upcoming",
            component: UpcomingLiveComponent,
            data: {
              title:"Առաջիկա դասընթացներ"
            }
          },
          {
            path: "my",
            component: MyLiveComponent,
            data: {
              title:"Իմ դասընթացները"
            }
          },
          {
            path: "participated",
            component: ParticipatedLiveComponent,
            data: {
              title:"Իմ մասնակցած դասընթացները"
            }
          },
        ]
      },
      {
        path: "groups",
        component: GroupsComponent,
        data: {
          title:"Խմբեր"
        },
        children: [
          {
            path: "",
            redirectTo: 'my',
            pathMatch: 'full',
          },
          {
            path: "my",
            component: MyGroupsComponent,
            data: {
              title:"Իմ խմբերը"
            }
          },
          {
            path: "saved",
            component: SavedGroupsComponent,
            data: {
              title:"Իմ պահած խմբերը"
            }
          },
        ]
      },
      {
        path: "preferable",
        component: PreferableComponent,
        data: {
          title:"Նախընտրելի օգտատերեր"
        }
      },
      {
        path: "test",
        component: TestComponent,
        data: {
          title:"Թեստեր"
        },
        children: [
          {
            path: '',
            redirectTo: 'my',
            pathMatch: 'full',
          },
          {
            path: "my",
            component: MyTestComponent,
            data: {
              title:"Իմ Թեստեր"
            },
          },
          {
            path: "saved",
            component: SavedTestComponent,
            data: {
              title:"Պահած թեստեր"
            },
          },
          {
            path: "add",
            component: AddTestComponent,
            data: {
              title:"Ավելացնել թեստ"
            },
          }
        ]
      },
      {
        path: "settings",
        component: SettingsComponent,
        canDeactivate: [ExitSettingsGuard],
        data: {
          title:"Կարգավորումներ"
        }
      },
      {
        path: "calendar",
        component: CalendarComponent,
        data: {
          title:"Օրացույց"
        }
      },
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'disabled',
  })],
  exports: [RouterModule],
  providers: [ExitSettingsGuard]
})
export class AppRoutingModule { }
