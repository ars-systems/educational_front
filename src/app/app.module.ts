import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeHy from '@angular/common/locales/hy';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { BarRatingModule } from "ngx-bar-rating";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './containers/nav/nav.component';
import { HeaderComponent } from './containers/header/header.component';
import { FooterComponent } from './containers/footer/footer.component';
import { VideoSliderComponent } from './sliders/video-slider/video-slider.component';
import { HomeComponent } from './components/home/home.component';
import { GroupSliderComponent } from './sliders/group-slider/group-slider.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { PartnersComponent } from './sliders/partners/partners.component';
import { LoginComponent } from './components/user/login-reg/login/login.component';
import { RegComponent } from './components/user/login-reg/reg/reg.component';
import { LoginRegComponent } from './components/user/login-reg/login-reg.component';
import { MethodsEffects } from './store/effects/methods.effects';
import { UserComponent } from './components/user/user.component';
import { methods } from './store/reducers/methods.reducer';
import { NotificationsComponent } from './components/user/notifications/notifications.component';
import { MessagesComponent } from './components/user/messages/messages.component';
import { CardComponent } from './containers/card/card.component';
import { GroupCardComponent } from './containers/group-card/group-card.component';
import { MyMaterialComponent } from './components/user/material/my-material/my-material.component';
import { SavedMaterialComponent } from './components/user/material/saved-material/saved-material.component';
import { MaterialComponent } from './components/user/material/material.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { CalendarComponent } from './components/user/calendar/calendar.component';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

import { PickerModule } from '@ctrl/ngx-emoji-mart'
import { NgxEmojModule }  from  'ngx-emoj';

import { NgxPaginationModule } from 'ngx-pagination'; 
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PreferableComponent } from './components/user/preferable/preferable.component';
import { PreferableCardComponent } from './components/user/preferable/preferable-card/preferable-card.component';
import { AddMaterialComponent } from './components/user/material/add-material/add-material.component';
import { GroupsComponent } from './components/user/groups/groups.component';
import { MyGroupsComponent } from './components/user/groups/my-groups/my-groups.component';
import { SavedGroupsComponent } from './components/user/groups/saved-groups/saved-groups.component';
import { SearchPipe } from './pipes/search.pipe';
import { CoursesComponent } from './components/user/courses/courses.component';
import { UpcomingLiveComponent } from './components/user/courses/upcoming-live/upcoming-live.component';
import { AddGroupComponent } from './components/user/groups/add-group/add-group.component';
import { ParticipatedLiveComponent } from './components/user/courses/participated-live/participated-live.component';
import { MyLiveComponent } from './components/user/courses/my-live/my-live.component';
import { AddLiveComponent } from './components/user/courses/add-live/add-live.component';
import { AddPreferableUserComponent } from './components/user/preferable/add-preferable-user/add-preferable-user.component';
import { ChatComponent } from './components/user/messages/chat/chat.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { TestComponent } from './components/user/test/test.component';
import { MaterialModule } from './modules/material.module';
import { AddTestComponent } from './components/user/test/add-test/add-test.component';
import { SavedTestComponent } from './components/user/test/saved-test/saved-test.component';
import { MyTestComponent } from './components/user/test/my-test/my-test.component';
import { SingleTestComponent } from './components/user/test/single-test/single-test.component';

const config: InputFileConfig = {
  fileAccept: '*',
  fileLimit: 4
};

registerLocaleData(localeHy);

@NgModule({
  declarations: [
    AppComponent,
    SearchPipe,

    HomeComponent,
    NavComponent,
    HeaderComponent,
    FooterComponent,
    VideoSliderComponent,
    GroupSliderComponent,
    DiagramComponent,
    PartnersComponent,
    LoginComponent,
    RegComponent,
    LoginRegComponent,
    UserComponent,
    NotificationsComponent,
    MessagesComponent,
    CardComponent,
    GroupCardComponent,
    MyMaterialComponent,
    SavedMaterialComponent,
    MaterialComponent,
    SettingsComponent,
    CalendarComponent,
    PreferableComponent,
    PreferableCardComponent,
    AddMaterialComponent,
    GroupsComponent,
    MyGroupsComponent,
    SavedGroupsComponent,
    CoursesComponent,
    UpcomingLiveComponent,
    AddGroupComponent,
    ParticipatedLiveComponent,
    MyLiveComponent,
    AddLiveComponent,
    AddPreferableUserComponent,
    ChatComponent,
    TestComponent,
    AddTestComponent,
    SavedTestComponent,
    MyTestComponent,
    SingleTestComponent,
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    PickerModule,
    NgxEmojModule,
    ScrollDispatchModule,
    InputFileModule.forRoot(config),
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    FileUploadModule,
    TextFieldModule,
    AppRoutingModule,
    BarRatingModule,
    StoreModule.forRoot({ 
      _methods: methods,
    }),
    EffectsModule.forRoot([MethodsEffects]),
  ],
  entryComponents:[
    AddMaterialComponent,
    AddGroupComponent,
    AddPreferableUserComponent,
    AddLiveComponent,
  ],
  providers: [
    Title,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }