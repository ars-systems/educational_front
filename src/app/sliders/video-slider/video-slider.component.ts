import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, HostListener, EventEmitter, Renderer2, ViewChildren, QueryList } from '@angular/core';
import Swiper from 'swiper'

@Component({
  selector: 'app-video-slider',
  templateUrl: './video-slider.component.html',
  styleUrls: ['./video-slider.component.scss']
})
export class VideoSliderComponent implements OnInit, AfterViewInit {

  @ViewChild('slider') slider: ElementRef;
  @ViewChild('pagination') pagination: ElementRef;
  @ViewChildren('video') video: QueryList<ElementRef>;

  count: number = 0;

  private videos = [
    {
      src: "/assets/video/slider/1.mp4"
    },
    {
      src: "/assets/video/slider/2.mp4"
    },
  ]

  constructor(private renderer: Renderer2) { }

  triggerVideo() {
    this.video.forEach((element) => {
      (element.nativeElement as HTMLVideoElement).addEventListener('mousemove', () => {
        (element.nativeElement as HTMLVideoElement).play();
      });
      (element.nativeElement as HTMLVideoElement).addEventListener('mouseleave', () => {
        (element.nativeElement as HTMLVideoElement).pause();
      })
    });

  }

  videoSlider() {
    let swiper = new Swiper(this.slider.nativeElement, {
      loop: true,
      slidesPerView: 1,
      pagination: {
        el: this.pagination.nativeElement,
        dynamicBullets: true,
      },
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.videoSlider()
    this.video.first.nativeElement.setAttribute('autoplay', '')
    this.triggerVideo()
  }

}