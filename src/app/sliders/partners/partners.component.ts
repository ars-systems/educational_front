import { Component, OnInit, ElementRef, Renderer2, ViewChild, AfterViewInit, Input } from '@angular/core';
import Swiper from 'swiper'

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit , AfterViewInit{

  @ViewChild('container') container: ElementRef;

  data = [
    {
      img:"/assets/img/partners/1.png",
    },
    {
      img:"/assets/img/partners/2.png",
    },
    {
      img:"/assets/img/partners/3.png",
    },
    {
      img:"/assets/img/partners/4.png",
    },
  ]

  swiper() {
    const swiper = new Swiper(this.container.nativeElement, {
      slidesPerView: 4,
      loop: true,
      breakpoints: {
        1024: {
          slidesPerView: 3,
        },
        768: {
          slidesPerView: 2,
        },
      }
    })
  }

  constructor(private elem: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
  }
  
  ngAfterViewInit(){
    this.swiper()
  }

}