import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Swiper from 'swiper'
import init from 'swiper'

@Component({
  selector: 'app-group-slider',
  templateUrl: './group-slider.component.html',
  styleUrls: ['./group-slider.component.scss']
})
export class GroupSliderComponent implements OnInit, AfterViewInit {

  @ViewChild('slider') slider: ElementRef;

  private groups = [
    {
      name:"Ծրագրավորողների խումբ",
      materialMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img:"./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name:"Բժիշկների խումբ",
      materialMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img:"./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name:"Ուսուցիչների խումբ",
      materialMember: 235,
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img:"./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name:"Տաքսիստների խումբ",
      materialMember: 235,
      text: "Խումբը ստեղծված է տաքսիստների համար",
      img:"./assets/img/group/4.jpg",
      rating: 5,
    },{
      name:"Դայակների խումբ",
      materialMember: 235,
      text: "Խումբը ստեղծված է դայակների համար",
      img:"./assets/img/group/5.jpg",
      rating: 5,
    },
  ];

  constructor() { }

  groupSlider() {
    var sliderSelector = this.slider.nativeElement,
      options = {
        loop: true,
        mousewheel: true,
        slidesPerView: 4,
        spaceBetween: -120,
        keyboard: {
          enabled: true,
        },
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        breakpoints: {
          2000: {
            slidesPerView: 4,
            spaceBetween: -100
          },
          768: {
            slidesPerView: 2.5,
            spaceBetween: -50
          },
          600: {
            slidesPerView: 1,
            spaceBetween: 0
          }
        },
        clickable:true,
        speed: 800,
        centeredSlides: true,
        effect: 'coverflow',
        grabCursor: true,
      };
    var mySwiper = new Swiper(sliderSelector, options);
  }

  ngOnInit() {
  }
  
  ngAfterViewInit(){
    this.groupSlider()
  }

}