import * as  Actions from '../actions/methods.actions'
export type ActionT = Actions.All;

export const initialState = {
  loginModal: null,
  logOrReg: null,
  user: null,
  preferableUsers: [],
  userToken: null,
  calendarData: [],
};

export function methods(state = initialState, action: ActionT) {
  // console.log(action['type'], action['payload'])
  switch (action.type) {
    case Actions.MethodsActionTypes.loginModal:
      return { ...state, loginModal: action.payload }
    case Actions.MethodsActionTypes.logOrReg:
      // console.log(action.payload)
      return { ...state, logOrReg: action.payload }
    case Actions.MethodsActionTypes.user:
      return { ...state, user: action.payload }
    case Actions.MethodsActionTypes.preferableUsers:
      return { ...state, preferableUsers: action.payload }
    case Actions.MethodsActionTypes.userToken:
      return { ...state, userToken: action.payload }
    case Actions.MethodsActionTypes.calendarData:
      return { ...state, calendarData: action.payload }
    default:
      return state;
  }
}
