import { Action } from '@ngrx/store';

export enum MethodsActionTypes {
  loginModal = '[Methods] login modal',
  logOrReg = '[Methods] login or registration',
  user = '[Methods] user',
  preferableUsers = '[Methods] preferableUsers',
  userToken = '[Methods] userToken',
  calendarData = '[Methods] calendarData',
}

export class LoginModal implements Action {
  readonly type = MethodsActionTypes.loginModal;
  constructor(public payload: {}) { }
}
export class LoginorRegistration implements Action {
  readonly type = MethodsActionTypes.logOrReg;
  constructor(public payload: {}) { }
}
export class User implements Action {
  readonly type = MethodsActionTypes.user;
  constructor(public payload: {}) { }
}
export class PreferableUsers implements Action {
  readonly type = MethodsActionTypes.preferableUsers;
  constructor(public payload: {}) { }
}
export class UserToken implements Action {
  readonly type = MethodsActionTypes.userToken;
  constructor(public payload: {}) { }
}
export class CalendarData implements Action {
  readonly type = MethodsActionTypes.calendarData;
  constructor(public payload: {}) { }
}
export type All = LoginModal | LoginorRegistration | User | PreferableUsers | UserToken | CalendarData;
