import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(data, inputVal: string, args: string[]) {
    if(data.length === 0 || inputVal === ""){
      return data
    }
    return data.filter(
      (data) => {
        let valid:boolean[] = [];
        args.forEach(element => {
          let bool:boolean = String(data[element]).toLowerCase().indexOf(inputVal.toLowerCase()) !== -1
          valid.push(bool)
        });

        if(valid.includes(true)){
          return true
        }
        else{
          return false
        }

      }
    )
  }

}
