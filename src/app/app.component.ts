import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Title } from '@angular/platform-browser';
import * as Actions from './store/actions/methods.actions'
import { UserService } from './services/user.service';
import { AppState } from './interface/interface';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  private loginModal: boolean;

  constructor(private titleService: Title, private _store: Store<AppState>, private userService: UserService, private router: ActivatedRoute, public currentRouter: Router) {
    setTimeout(() => {
      this.pageTitles()
    }, 0);
    currentRouter.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        setTimeout(() => {
          this.pageTitles()
        }, 0);
      }
    })
    this._store.pipe(select('_methods')).subscribe(res => {
      this.loginModal = res.loginModal
    })
  }

  pageTitles() {
    const config = this.router.snapshot.children[0];
    var title: string = config.data['title'];
    var titleG = (config): string => {
      if (config.children[0]) {
        title = config.children[0].data['title'] + " | " + title;
        titleG(config.children[0])
        return title
      }
      else {
        return title
      }
    }
    this.titleService.setTitle(titleG(config))

  }

  ngOnInit() {
    let token = localStorage.getItem('user')
    this._store.dispatch(new Actions.UserToken(token))
    if (token) {
      this.userService.sendToken()
        .subscribe(res => {
          let userData = res['data'][0];
          this._store.dispatch(new Actions.User(userData))
        }, error => {
          console.log("error", error)
        })
    }
  }

}
