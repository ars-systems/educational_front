import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatOptionModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule, MatButtonToggleModule, MatDialogModule, MatSelectModule, MatChipsModule, MatAutocompleteModule, MatIconModule, MatFormFieldModule, MatRadioModule, MatCheckboxModule, MatTabsModule, MatTooltipModule, MatCardModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  providers: [MatDatepickerModule],
  declarations: [],
  bootstrap: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatFormFieldModule,
    MatRadioModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTooltipModule,
    MatCardModule,
    DragDropModule,
  ],
  exports: [
    BrowserAnimationsModule,
    CommonModule,
    MatInputModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatFormFieldModule,
    MatRadioModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTooltipModule,
    MatCardModule,
    DragDropModule,
  ]
})
export class MaterialModule { }
