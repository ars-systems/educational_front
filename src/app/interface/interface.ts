// Store Interface
export interface AppState {
}

// Site Navigation Interface
export interface INav {
    name?: string;
    icon?: string;
    href?: string;
    subMenu?: INav[];
}[]

// Diagram Interface
export interface IDiagram {
    name?: string,
    percent?: number,
    color?: string
}

// Card Interface
export interface ICardItem {
    name: string;
    materialMember: number;
    img: string;
    rating: number;
}

// User page dashboard tabs Interface
export interface ITab {
    name: string,
    icon: string,
    href: string
}

// User Interface
export interface IUser {

}

// Notifications Interface
export interface INotification {
    img: string,
    name: string,
    notifications?: {
        name: string,
        img: string,
        materialMember?: number,
        type?: string,
        text?: string,
        rating?: number,
    }[]
}

// Preferable users interface
export interface IPreferableUsers {

}


export interface IQuestion {
    type?: number;
    ask?: string;
    options?: IAnswerOptions[];
    pic?: File;
    answer?: string;
    error?: {}
};

export interface IAnswerOptions {
    label: string;
    checked: boolean
}


export interface ITest {
    name?: string;
    description?: string;
    file?: File;
    secureVal?: number;
    questions?: IQuestion[]
}