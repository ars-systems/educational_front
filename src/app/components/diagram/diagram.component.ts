import { Component, ElementRef, Input, ViewChild, AfterViewInit } from '@angular/core';
import { IDiagram } from 'src/app/interface/interface';

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss'],
})
export class DiagramComponent implements AfterViewInit{

  @ViewChild('canvas') canvas: ElementRef ;
  @Input() canv: IDiagram;

  private time;
  private context;
  private al = 0;
  private start = 4.72;
  private cw;
  private ch;
  private diff;

  circle(canv, percent,color){
      this.context = canv.getContext('2d')
      this.diff = (this.al / 100) * Math.PI * 2;
      this.context.clearRect(0, 0, 400, 200);
      this.context.beginPath();
      this.context.arc(this.cw, this.ch, 60, 0, 2 * Math.PI, false);
      this.context.fillStyle = 'transparent';
      this.context.fill();
      this.context.strokeStyle = '#c4cfcf';
      this.context.stroke();
      this.context.fillStyle = color || '#424242';
      this.context.strokeStyle = color || '#424242';
      this.context.textAlign = 'center';
      this.context.lineWidth = 3;
      this.context.font = '20pt sans-serif';
      this.context.beginPath();
      this.context.arc(this.cw, this.ch, 60, this.start, this.diff + this.start, false);
      this.context.stroke();
      this.context.fillText(this.al + '%', this.cw + 2, this.ch + 6);
      if (this.al >= percent) {
        clearTimeout(this.time);
      }
      this.al++;
  }

  progress(canv,percent,color){
    this.context = canv.getContext('2d');
    this.cw = this.context.canvas.width / 2;
    this.ch = this.context.canvas.height / 2;
    this.time = setInterval(()=>{
      return this.circle(canv,percent,color)
    }, 20);
  }

  constructor() { 
  }
  
  ngAfterViewInit(){
    this.progress(this.canvas.nativeElement, this.canv.percent, this.canv.color)
  }

}
