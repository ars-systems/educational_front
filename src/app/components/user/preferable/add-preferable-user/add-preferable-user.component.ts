import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { UrlsService } from 'src/app/services/urls.service';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/interface/interface';
import * as Actions from '../../../../store/actions/methods.actions'

@Component({
  selector: 'app-add-preferable-user',
  templateUrl: './add-preferable-user.component.html',
  styleUrls: ['./add-preferable-user.component.scss']
})
export class AddPreferableUserComponent implements OnInit {

  p: number = 1;
  private selectedType: string = "all"
  private types = [
    { label: "Բոլոր", value: "all" },
    { label: "Դասախոս", value: "lecturer" },
    { label: "Ուսանող", value: "student" },
    { label: "Այլ", value: "other" }
  ]

  private unSub: Subscription;
  private users;

  constructor(public dialogRef: MatDialogRef<AddPreferableUserComponent>, private userService: UserService, private urls: UrlsService, private _store: Store<AppState>) {
    this._store.pipe(select('_methods')).subscribe(res => {
    })
  }

  private uploadUrl: string = this.urls.uploadUrl;

  close(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    window.addEventListener('resize', () => {
      this.modalHeight()
    })
    window.dispatchEvent(new Event('resize'))
    this.getData()
  }

  modalHeight() {
    let elm: HTMLElement = document.querySelector('mat-dialog-container')
    setTimeout(() => {
      if (elm) {
        if (window.outerHeight < elm.scrollHeight) {
          elm.classList.add('res')
        }
        else {
          elm.classList.remove('res')
        }
      }
    });
  }

  addUser(idx: number) {
    this.userService.addPreferableUser(this.users[idx]['id'])
      .subscribe(res => {
        this.users[idx].added = true;
        this._store.dispatch(new Actions.PreferableUsers(res['data']))
      }, error => {
        console.log(error)
      })
  }

  getData() {
    this.unSub = this.userService.allUsersNotFriend(this.p).subscribe(data => {
      this.users = data['data']
    })

    if (this.users) {
      this.unSub.unsubscribe()
    }
  }

}