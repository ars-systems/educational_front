import { Component, OnInit, HostListener } from '@angular/core';
import { IPreferableUsers, AppState } from 'src/app/interface/interface';
import { MatDialog } from '@angular/material';
import { AddPreferableUserComponent } from './add-preferable-user/add-preferable-user.component';
import { UserService } from 'src/app/services/user.service';
import { Store, select } from '@ngrx/store';
import * as Actions from 'src/app/store/actions/methods.actions';

@Component({
  selector: 'app-preferable',
  templateUrl: './preferable.component.html',
  styleUrls: ['./preferable.component.scss']
})
export class PreferableComponent implements OnInit {

  private p: number = 1;
  private inputVal: string = '';
  private preferableUsers: IPreferableUsers[] = [];

  private newMaterial: string;

  constructor(private dialog: MatDialog, private userService: UserService, private _store: Store<AppState>) {
    this._store.pipe(select('_methods')).subscribe(res => {
      this.preferableUsers = res.preferableUsers
    })
  }

  ngOnInit() {
    this.getFriends()
  }

  getFriends() {
    this.userService.getFriend(this.p).subscribe(res => {
      this.preferableUsers = res['data']
      this._store.dispatch(new Actions.PreferableUsers(this.preferableUsers))
    }, error => {
      console.log(error)
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddPreferableUserComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newMaterial = result;
    });
  }

}
