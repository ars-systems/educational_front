import { Component, OnInit, Input } from '@angular/core';
import { UrlsService } from 'src/app/services/urls.service';
import { UserService } from 'src/app/services/user.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/interface/interface';
import * as Actions from 'src/app/store/actions/methods.actions'

@Component({
  selector: 'app-preferable-card',
  templateUrl: './preferable-card.component.html',
  styleUrls: ['./preferable-card.component.scss']
})
export class PreferableCardComponent implements OnInit {

  @Input() item;
  private url: string;

  constructor(private urls: UrlsService, private userService: UserService, private _store:Store<AppState>) {
    this.url = this.urls.uploadUrl;
  }

  ngOnInit() {
  }

  removeUser(id: number) {
    this.userService.removeFriend(id)
      .subscribe(res=>{
        this._store.dispatch(new Actions.PreferableUsers(res['data']))
      })
  }

}
