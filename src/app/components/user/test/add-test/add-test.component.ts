import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { CdkDragDrop, moveItemInArray, CdkDragEnter, CdkDragSortEvent, CdkDragStart } from '@angular/cdk/drag-drop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestComponent } from '../test.component';
import { TestService } from 'src/app/services/test.service';
import { ITest } from 'src/app/interface/interface';


@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.scss']
})
export class AddTestComponent implements OnInit {

  private quiz: ITest = {

  };
  private err: boolean = false;
  private firstAdd = false;
  private testList: any[] = [];
  @ViewChildren('drag') drag: QueryList<ElementRef>;

  dropQuestions(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.testList, event.previousIndex, event.currentIndex);
  }

  private testForm: FormGroup;

  constructor(private parentComp: TestComponent, private testService: TestService, private fb: FormBuilder) { }

  private initForm(): void {
    this.testForm = this.fb.group({
      name: [null, [
        Validators.required,
      ]],
      description: [null, [
        Validators.required,
      ]],
      secureVal: ['1', [
        Validators.required,
      ]],
      file: [null, [
        Validators.required
      ]],
    })
  }

  private sortList() {
    this.testList.forEach((elm, id) => {
      elm.id = id;
      elm.question = {}
    });
    if (this.testList.length === 1) {
      setTimeout(() => {
        let drag: HTMLElement = document.querySelector('mat-icon[cdkDragHandle]');
        if (drag)
          drag.style.pointerEvents = 'none'
      }, 0);
    }
    else {
      setTimeout(() => {
        let drag: HTMLElement = document.querySelector('mat-icon[cdkDragHandle]');
        if (drag)
          drag.style.pointerEvents = 'unset'
      }, 0);
    }
  }

  private newTest(index: number) {
    Array.prototype['insert'] = function (index, item) {
      this.splice(index, 0, item);
    };
    if (this.firstAdd) {
      this.testService.addingTest(this.testList)
      this.err = this.testService.haveErr
      if (this.err) {
        // error
        setTimeout(() => {
          const errElmTop: HTMLDivElement = document.querySelector('.test.error');
          window.scroll(0, errElmTop.offsetTop + 100)
        });
      }
      else {
        this.testList['insert'](index + 1, {});
        this.sortList();
      }
    }
    else {
      this.testList['insert'](index + 1, {});
      this.firstAdd = true;
      this.sortList();
    }

  }

  private deleteTest(index: number) {
    this.testList.splice(index, 1)
    if (this.testList.length === 0) {
      this.firstAdd = false;
    }
    this.sortList()
  }

  private auto_grow = this.parentComp.auto_grow

  ngOnInit() {
    this.initForm()
  }

  finall() {
    this.testService.addingTest(this.testList);
    const err = this.testService.haveErr;
    const controls = this.testForm.controls
    if (this.testForm.invalid) {
      window.scroll(0, 0)
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
      if (!err) {
        // send
        this.quiz = {
          name: controls['name'].value,
          description: controls['description'].value,
          file: controls['file'].value[0].file,
          secureVal: controls['secureVal'].value,
          questions: this.testList
        }
        console.log(this.quiz)
      }
      else {
        // error
        setTimeout(() => {
          const errElmTop: HTMLDivElement = document.querySelector('.test.error');
          window.scroll(0, errElmTop.offsetTop + 100)
        });
      }
    }

  }

}
