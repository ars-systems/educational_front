import { Component, OnInit, Input, ViewChild, ElementRef, KeyValueDiffers, KeyValueDiffer, DoCheck, IterableDiffers, IterableDiffer, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TestComponent } from '../test.component';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { IQuestion } from 'src/app/interface/interface';
import { isObject } from 'util';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-single-test',
  templateUrl: './single-test.component.html',
  styleUrls: ['./single-test.component.scss']
})
export class SingleTestComponent implements OnInit, DoCheck, OnChanges {

  @Input() test;

  private file = null;
  private answerLong: string = "";
  private selectedIndex: number = 0;
  private haveError: boolean = false;
  private selectedType: 'single' | 'multiple' = 'single';
  private ask: string = "";
  private selectedAnswer: null | boolean = null;

  private options = [
    { checked: false, label: 'Տարբերակ' },
  ]
  // question data
  private question: IQuestion = {
    type: 0,
    ask: this.ask,
    options: this.options,
  }

  // drag drop and sorting answers
  dropAnswers(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.options, event.previousIndex, event.currentIndex);
    this.changeData()
  }

  ngOnChanges(changes: SimpleChanges) {
    this.changeData()
  }

  // worked when changed TABs, choosed type for QA
  private QA(idx: number) {
    delete this.question.error;
    this.question.error = {};

    this.question.type = idx;
    this.question.type = this.selectedIndex;
    if (idx === 0) {
      if (this.file && this.file[0] != null) {
        this.question.pic = this.file[0].file
      }
      else {
        this.question.pic = null
      }
    }
    else {
      delete this.question['pic'];
    }
    if (idx === 2) {
      delete this.question['options'];
      this.question.answer = this.answerLong
    }
    else {
      this.question.options = this.options
      delete this.question['answer'];
    }

  }

  private onChange(event) {
    this.options.forEach((item) => {
      item.checked = false;
    })
  }

  // Adding new Answer
  private addAnswer(index: number) {
    Array.prototype['insert'] = function (index, item) {
      this.splice(index, 0, item);
    };
    if (this.options.length < 5) {
      this.options['insert'](index + 1, { checked: false, label: 'Տարբերակ' });
    }
    this.changeData()
  }

  // deleting Answer
  private deleteAnswer(index: number) {
    if (this.options.length > 1) {
      this.options.splice(index, 1)
    }
    else {
      this.options[0].label = '';
    }
    this.changeData()
  }


  constructor(private parentComp: TestComponent, private fb: FormBuilder, private testService: TestService) {
  }

  ngOnInit() {
  }

  ngDoCheck(): void {
    this.haveError = Object.keys(this.question.error).length != 0;
  }

  private auto_grow = this.parentComp.auto_grow

  radioChange(id: any) {
    this.options.forEach((element, idx) => {
      if (idx === id) {
        element.checked = true;
      }
      else {
        element.checked = false;
      }
    });
  }

  changeTabs() {
    this.QA(this.selectedIndex)
  }

  changeData() {
    this.QA(this.selectedIndex)
    this.question.ask = this.ask;
    this.test.question = this.question;
  }

}
