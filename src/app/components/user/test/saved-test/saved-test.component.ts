import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saved-test',
  templateUrl: './saved-test.component.html',
  styleUrls: ['./saved-test.component.scss','../my-test/my-test.component.scss']
})
export class SavedTestComponent implements OnInit {


  private savedTests = [
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      author: "Կարապետ Հակոբյան",
      img: "./assets/img/group/2.jpg",
      rating: 4.5,
    },
 
  ]


  constructor() { }

  ngOnInit() {
  }

}
