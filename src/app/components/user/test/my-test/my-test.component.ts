import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-test',
  templateUrl: './my-test.component.html',
  styleUrls: ['./my-test.component.scss']
})
export class MyTestComponent implements OnInit {

  private myTests = [
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ինֆորմատիկայի թեստ",
      materialMember: 235,
      img: "./assets/img/group/2.jpg",
      rating: 4.5,
    },
 
  ]


  constructor() { }

  ngOnInit() {
  }

}
