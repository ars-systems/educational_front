import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  private tabList = [
    {
      name: "Իմ ստեղծած թեստերը",
      href: "my",
    },
    {
      name: "Իմ նախընտրած թեստերը",
      href: "saved",
    },
  ]

  constructor() { }

  ngOnInit() {
  }

  public auto_grow(element) {
    element.srcElement.style.height = "5px";
    element.srcElement.style.height = (element.srcElement.scrollHeight) + "px";
  }

}