import { Component, ViewEncapsulation } from '@angular/core';
import { startOfDay, isSameMonth } from 'date-fns';
import { Subject, Subscription } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarEventTitleFormatter } from 'angular-calendar';
import { CalendarSelectedDayService } from 'src/app/services/calendarSelectedDay.service';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/interface/interface';
import * as Actions from 'src/app/store/actions/methods.actions'
import { CustomCalendarDayView } from './custom_calendar_day_view';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomCalendarDayView
    }
  ]
})
export class CalendarComponent {

  private unSub: Subscription;
  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;
  viewDate: Date = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="material-icons">close</i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: any = [];

  activeDayIsOpen: boolean = false;
  
  constructor(private selectedDay: CalendarSelectedDayService, private _store: Store<AppState>) {
    this.unSub = this.selectedDay.day.subscribe(day => {
      if (this.viewDate.toString() != day.toString()) {
        this.viewDate = day
        this.activeDayIsOpen = true;
      }
    })
    this._store.pipe(select('_methods')).subscribe(res => {
      this.events = res.calendarData
    })
  }

  weekStartsOn: number = 1;

  dayClicked({ date, events }: { date: Date; events: any }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      this.activeDayIsOpen = !this.activeDayIsOpen
    }
  }

  changeTime(){
    this.activeDayIsOpen = false
    return this.refresh.next()
  }

  eventTimesChanged({ event, newStart }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
        };
      }
      return iEvent;
    });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'Նոր իրադարձություն',
        start: startOfDay(this.viewDate),
        draggable: false,
        actions: this.actions,
      }
    ];
    this._store.dispatch(new Actions.CalendarData(this.events))
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

}
