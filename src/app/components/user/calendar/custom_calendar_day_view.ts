import { LOCALE_ID, Inject } from '@angular/core';
import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
import { DatePipe } from '@angular/common';

export class CustomCalendarDayView extends CalendarEventTitleFormatter {
  constructor(@Inject(LOCALE_ID) private locale: string) {
    super();
  }

  month(event: CalendarEvent): string {
    return `<span>${new DatePipe(this.locale).transform(
      event.start,
      'hh:mm a',
      this.locale
    )} | </span> ${event.title}`;
  }

}
