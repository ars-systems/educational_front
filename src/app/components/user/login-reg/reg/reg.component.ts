import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Subscription, Observable } from 'rxjs';
import { EmailValidateService } from 'src/app/services/email-validate.service';

@Component({
  selector: 'app-reg',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg.component.scss', '../login/login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegComponent implements OnInit {

  @ViewChild('submit') submit: ElementRef;

  private registred: boolean = false;
  private hide: boolean = true;
  private userForm: FormGroup;
  private userAdd: Subscription;
  private roleValue: boolean = false;
  private roles: {}[] = [
    {
      label: "Դասախոս",
      value: "lecturer",
    },
    {
      label: "Ուսանող",
      value: "student",
    },
    {
      label: "Այլ",
      value: "other",
    }
  ]

  constructor(private fb: FormBuilder, private userService: UserService, private renderer: Renderer2, private emailValidateService: EmailValidateService) { }

  ngOnInit() {
    this.initForm()

    if (this.userAdd) {
      this.userAdd.unsubscribe()
    }
  }

  private initForm(): void {
    this.userForm = this.fb.group({
      name: [null, [
        Validators.required,
        Validators.pattern(/[A-z0-9]*/),
        Validators.minLength(3)
      ]],
      email: [null, [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
      ], [
          this.nameAsyncValidator.bind(this)
        ]
      ],
      password: [null, [
        Validators.required,
        this.passwordValidator,
      ]],
      repeatPassword: [null, [
        Validators.required,
        this.checkPasswords
      ],
      ],
      gender: [null, [
        Validators.required,
      ]],
      role: [null, [
        Validators.required,
        this.changeRole.bind(this)
      ]],
      otherName: [null, [
      ]],
    })
  }

  private isControlInvalid(controlName: string): boolean {
    const control = this.userForm.controls[controlName]
    let result = control.invalid && control.touched
    return result
  }

  private nameAsyncValidator(control: FormControl): Observable<ValidationErrors> {
    return this.emailValidateService.validateName(control.value);
  }


  changeRole(control: FormControl) {
    const val = control.value;
    this.roleValue = (val == 'other') ? true : false;
    if (control.parent) {
      const otherName: FormControl = control.parent.controls['otherName'];
      if (val == 'other') {
        const newValid: ValidatorFn[] = [
          Validators.required,
        ];
        otherName.setValidators(newValid)
      }
      else {
        otherName.clearValidators()
      }
      otherName.updateValueAndValidity()
    }
  }

  private rePassValid(rePassInp: HTMLInputElement) {
    if (this.userForm.controls) {
      let pass: FormControl = (this.userForm.controls.password as FormControl);
      let rePass: FormControl = (this.userForm.controls.repeatPassword as FormControl);

      if (pass.value == '') {
        rePassInp.value = ''
        setTimeout(() => {
          this.userForm.controls['repeatPassword'].setErrors(null)
          rePass.markAsUntouched()
        });
      }
      else {
        const result = (rePass.touched && pass.value == rePassInp.value)
        if (!result) {
          setTimeout(() => this.userForm.controls['repeatPassword'].setErrors({ 'invalid': true }));
        }
        else {
          setTimeout(() => this.userForm.controls['repeatPassword'].setErrors(null));
        }
      }
    }
  }

  private checkPasswords(control: FormControl) {
    if (control.parent) {
      const pass = control.parent.controls['password']
      const result = control.untouched || pass.untouched || (control.touched && pass.touched && control.value === pass.value);
      if (!result) {
        return { invalid: "Գաղտնաբառերը չեն համընկնում !" }
      }
      return null;
    }
  }

  private passwordValidator(control: FormControl) {
    const value = control.value;
    const hasNum = /[0-9]/.test(value)
    const hasCapitalLetter = /[A-Z]/.test(value)
    const hasLowerCaseLetter = /[a-z]/.test(value)
    const isLengthValid = value ? value.length >= 6 : false;
    const passValid = hasNum && hasCapitalLetter && hasLowerCaseLetter && isLengthValid

    if (!passValid) {
      return { invalid: "Գաղտնաբառը պետք է պարունակի առնվազն 6նիշ, ունենա առնվազն 1 մեծատառ, փոքրատառեր և թվեր! !" }
    }
    return null;
  }

  sendReg() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
      this.registred = true;
      this.userAdd = this.userService
        .addUser(this.userForm.value)
        .subscribe(data => data)
    }
  }
}
