import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as Actions from '../../../store/actions/methods.actions'
import { AppState } from 'src/app/interface/interface';

@Component({
  selector: 'app-login-reg',
  templateUrl: './login-reg.component.html',
  styleUrls: ['./login-reg.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginRegComponent implements OnInit {

  @ViewChild('modal') modal:ElementRef;

  private logOrReg: "signIn" | "signUp" = "signIn";
  private registred:boolean;

  constructor(private _store: Store<AppState>, private renderer: Renderer2) { 
    this._store.pipe(select('_methods')).subscribe(res=>{
      this.logOrReg = res.logOrReg
      this.registred = res.registred
    })
  }
  
  ngOnInit() {
    this.modal.nativeElement.addEventListener('click',(event)=>{
      if (!event.target.closest(".modal")) {
        this._store.dispatch(new Actions.LoginModal(false))
      }
    })
  }
}
