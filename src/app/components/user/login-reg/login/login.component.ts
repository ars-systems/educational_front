import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { Store } from '@ngrx/store';
import * as Actions from '../../../../store/actions/methods.actions'
import { Router } from '@angular/router';
import { AppState } from 'src/app/interface/interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  private loginError:boolean;
  private hide: boolean = true;
  private userForm: FormGroup;
  private userLogin: Subscription;

  @ViewChild('submit') submit: ElementRef;

  constructor(private fb: FormBuilder, private userService: UserService, private _store: Store<AppState>, private router: Router) { }

  ngOnInit() {
    this.initForm()
    if (this.userLogin) {
      this.userLogin.unsubscribe()
    }
  }

  private initForm(): void {
    this.userForm = this.fb.group({
      email: [null, [
        Validators.required,
        Validators.email,
        Validators.minLength(1)
      ]],
      password: [null, [
        Validators.required,
        Validators.minLength(6)
      ]],
      rememberMe: [null],
    })
  }

  login() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
      this.userLogin = this.userService.login(this.userForm.value)
        .subscribe((data: string) => {
          if (data['status'] == true) {
            const user = data['data'][0]
            this._store.dispatch(new Actions.LoginModal(false))
            this._store.dispatch(new Actions.User(user.user))
            this._store.dispatch(new Actions.UserToken(user.token))
            this.router.navigate(['/user']);
            localStorage.setItem('user', user.token);
          }
          else {
            this.userForm.reset()
            this.userForm.controls['email'].markAsTouched()
            this.userForm.controls['password'].markAsTouched()
            this.loginError = true;
          }
        })
    }
  }
}