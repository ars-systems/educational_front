import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { isSameMonth } from 'date-fns';
import { Subscription } from 'rxjs';
import { CalendarView } from 'angular-calendar';
import { IUser, AppState } from 'src/app/interface/interface';
import { UserService } from 'src/app/services/user.service';
import * as Actions from '../../store/actions/methods.actions'
import { UrlsService } from 'src/app/services/urls.service';
import { CalendarSelectedDayService } from 'src/app/services/calendarSelectedDay.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @ViewChild('nav') nav: ElementRef;

  private sizeTablet: boolean = false;

  private user: IUser;
  private userRate = 4;

  private statistics = [
    [
      {
        name: "Հետևորդ",
        count: "24",
        color: "red"
      },
      {
        name: "Խմբեր",
        count: "3",
        color: "green"
      },
    ],
    [
      {
        name: "Հետևում եմ",
        count: "8",
        color: "#424242"
      },
      {
        name: "Թեստեր",
        count: "17",
        color: "yellow"
      },
    ],
    [
      {
        name: "Նյութ",
        count: "3",
        color: "blue"
      },
      {
        name: "Օնլայն դասեր",
        count: "1",
        color: "black"
      },
    ]
  ]
  private tabList = [
    {
      name: "Ծանուցումներ",
      icon: "home",
      href: "notification",
    },
    {
      name: "Իմ նամակները",
      icon: "home",
      href: "message",
    },
    {
      name: "Իմ նյութերը",
      icon: "home",
      href: "material",
    },
    {
      name: "Իմ խմբերը",
      icon: "home",
      href: "groups",
    },
    {
      name: "Իմ թեստերը",
      icon: "home",
      href: "test",
    },
    {
      name: "Իմ օնլայն դասընթացները",
      icon: "home",
      href: "courses",
    },
    {
      name: "Նախընտրելի օգտատերեր",
      icon: "home",
      href: "preferable",
    },
    {
      name: "Օրացույց",
      icon: "home",
      href: "calendar",
    },
    {
      name: "Խմբագրել էջը",
      icon: "home",
      href: "settings",
    },
    {
      name: "Ելք",
      icon: "home",
      href: "",
    },
  ]

  private unSub: Subscription;
  private events: any[] = [];
  private viewDate: Date = new Date();
  private view: CalendarView = CalendarView.Month;

  constructor(private _store: Store<AppState>, private selectedDay: CalendarSelectedDayService, private router: Router, private userService: UserService, private urls: UrlsService) {
    this.unSub = this._store.pipe(
      select('_methods')).subscribe(res => {
        this.user = res.user
        if (!res.userToken) {
          this.router.navigate(['']);
        }
        this.events = res.calendarData
      })

    if (this.user) {
      this.unSub.unsubscribe()
    }
  }

  private uploadUrl: string = this.urls.uploadUrl;

  logout() {
    const realy = window.confirm("Հաստատ ուզում էս դուրս գաս ՞՞՞")
    if (realy) {
      localStorage.removeItem('user')
      this._store.dispatch(new Actions.User(null))
      this.router.navigate([''])
    }
  }

  dayClicked({ date, events }: { date: Date; events: any }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      this.selectedDay.calendarSelectedDay(this.viewDate)
      this.router.navigate(['/user/calendar'])
    }
  }

  resize() {
    if (window.innerWidth <= 1100) {
      this.sizeTablet = true;
    }
    else {
      this.sizeTablet = false;
    }
  }

  ngOnInit() {
    window.addEventListener('resize', () => {
      this.resize()
    })
    window.dispatchEvent(new Event('resize'))
  }

}
