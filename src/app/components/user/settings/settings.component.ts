import { Component, OnInit, AfterViewInit, DoBootstrap, DoCheck } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ComponentCanDeactivate } from './exit.settings.guard';
import { Observable, Subscription } from 'rxjs';
import { ValidatorFn } from '@iplab/ngx-file-upload';
import { UserService } from 'src/app/services/user.service';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/interface/interface';
import * as Actions from '../../../store/actions/methods.actions'
import { UrlsService } from 'src/app/services/urls.service';
import { MatRadioGroup } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, ComponentCanDeactivate, DoCheck {

  private autocomplate = 'off';
  private count: number = 0;
  private passError: boolean = false;
  private edit: Subscription;
  private unSub: Subscription;
  private user;
  private bool: boolean = false;
  private readonly: boolean = true;
  private unSavedChanges: boolean = false;
  private gender = true;
  private roleValue: boolean = false;
  private file = '';

  canDeactivate(): boolean | Observable<boolean> {
    if (this.unSavedChanges) {
      return confirm("Դուք ունեք չպահպանված տվյալներ, վստահ եք ու ցանկանում եք լքել էջը ՞")
    }
    else {
      return true
    }
  }

  constructor(private fb: FormBuilder, private _store: Store<AppState>, private userService: UserService, private urls: UrlsService) {
    this.unSub = this._store.pipe(select('_methods')).subscribe(res => {
      this.user = res.user;
    })

  }

  uploadUrl = this.urls.uploadUrl;

  ngDoCheck() {
    if (this.user && this.count == 0) {
      this.unSub.unsubscribe()
      this.initForm(this.user)
      this.onChanges()
      this.bool = true
      this.count++
    }
  }

  private userForm: FormGroup;

  private initForm(user): void {
    this.userForm = this.fb.group({
      name: [user.name || '', [
        Validators.required,
        Validators.minLength(3)
      ]],
      surname: [user.surname || ''],
      email: [user.email || '', [
        Validators.required,
      ]],
      newPassword: ['', [
      ]],
      reNewPassword: ['', [
      ]],
      profession: [user.professions || ''],
      blockMassage: [user.blockMassage || false],
      university: [user.university || ''],
      work: [user.work || ''],
      gender: [user.gender, [
        Validators.required,
      ]],
      role: [user.role || null, [
        Validators.required,
        this.changeRole.bind(this)
      ]],
      otherName: [user.otherName || '', [
        Validators.required,
      ]],
      image: [
        [{
          preview: this.uploadUrl + this.user.image
        }]
      ],
      password: ['', [
        Validators.required,
      ]],
    })

  }


  onChanges(): void {
    let lastVal = this.userForm.value
    let valid = [];
    this.userForm.valueChanges.subscribe(val => {
      valid = []
      for (let i = 0; i <= Object.values(val).length; i++) {
        valid.push(Object.values(lastVal)[i] == Object.values(val)[i])
      }
      if (valid.includes(false)) {
        this.unSavedChanges = true;
      }
      else {
        this.unSavedChanges = false;
      }
    });
  }

  newPassValid(rePassInp: HTMLInputElement) {
    if (this.userForm.controls) {
      let newPass: FormControl = (this.userForm.controls.newPassword as FormControl);
      let reNewPass: FormControl = (this.userForm.controls.reNewPassword as FormControl);
      const newValid: ValidatorFn[] = [
        Validators.required,
      ]
      if (newPass.value == '') {
        rePassInp.value = ''
        reNewPass.clearValidators()
        setTimeout(() => {
          this.userForm.controls['reNewPassword'].setErrors(null)
          reNewPass.markAsUntouched()
        });
      }
      else {
        const result = (reNewPass.touched && newPass.value == rePassInp.value)
        reNewPass.setValidators(newValid)
        if (!result) {
          setTimeout(() => this.userForm.controls['reNewPassword'].setErrors({ 'invalid': true }));
        }
        else {
          setTimeout(() => this.userForm.controls['reNewPassword'].setErrors(null));
        }
        reNewPass.updateValueAndValidity()
      }
    }
  }

  changeRole(control: FormControl) {
    const val = control.value;
    this.roleValue = (val == 'other') ? true : false;
    if (control.parent) {
      const otherName: FormControl = control.parent.controls['otherName'];
      if (val == 'other') {
        const newValid: ValidatorFn[] = [
          Validators.required,
        ];
        otherName.setValidators(newValid)
      }
      else {
        otherName.clearValidators()
      }
      otherName.updateValueAndValidity()
    }

    return null
  }


  editUser() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
      this.edit = this.userService.editUser(this.userForm, this.user.image)
        .subscribe(res => {
          if (res['status'] == false) {
            this.passError = true;
            setTimeout(() => this.userForm.controls['password'].setErrors({ 'invalid': true }));
          }
          else {
            this.userForm.controls['password'].reset()
            this.unSavedChanges = false;
            this.passError = false;
            const user = res['data'][0].data
            this._store.dispatch(new Actions.User(user))
            window.scroll(0, 0)
          }
        }, error => {
          console.log("error", error)
        })
    }
  }

  ngOnInit() {
  }

}