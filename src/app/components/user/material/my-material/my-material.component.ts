import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddMaterialComponent } from '../add-material/add-material.component';

@Component({
  selector: 'app-my-material',
  templateUrl: './my-material.component.html',
  styleUrls: ['./my-material.component.scss']
})
export class MyMaterialComponent implements OnInit {

  private newMaterial: string;

  private selected = 'all';
  private myMaterial = [
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 4.5,
    },
 
  ]

  constructor(private dialog: MatDialog) { 
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddMaterialComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newMaterial = result;
    });
  }

  ngOnInit() {
  } 

}
