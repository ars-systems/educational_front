import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saved-material',
  templateUrl: './saved-material.component.html',
  styleUrls: ['./saved-material.component.scss','../my-material/my-material.component.scss']
})
export class SavedMaterialComponent implements OnInit {

  private selected = 'all';
  private savedMaterial = [
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      author: "Կարեն Գրիգորյան",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      author: "Կարեն Գրիգորյան",
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      materialMember: 235,
      type: "Թեստեր",
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      author: "Կարեն Գրիգորյան",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
  ]


  constructor() { }

  ngOnInit() {
  }

}
