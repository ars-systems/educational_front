import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FileUploadValidators, ValidatorFn } from '@iplab/ngx-file-upload';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-add-material',
  templateUrl: './add-material.component.html',
  styleUrls: ['./add-material.component.scss']
})
export class AddMaterialComponent implements OnInit {

  private hideInp: boolean = false;
  private userForm: FormGroup;
  private secureVal = '1';
  private sectors: {}[] = [
    {
      label: "Դասախոս",
      value: "lecturer",
    },
    {
      label: "Ուսանող",
      value: "student",
    },
    {
      label: "Այլ",
      value: "other",
    }
  ]

  private types: {}[] = [
    {
      label: "Թեզ",
      value: "tez",
    },
    {
      label: "Աուդիո գիրք",
      value: "audio",
    },
    {
      label: "Այլ",
      value: "other",
    }
  ]

  constructor(public dialogRef: MatDialogRef<AddMaterialComponent>, private fb: FormBuilder) { }

  private initForm(): void {
    this.userForm = this.fb.group({
      articleName: [null, [
        Validators.required,
      ]],
      authorName: [null, [
        Validators.required,
      ]],
      branch: [null, [
        Validators.required,
      ]],
      type: [null, [
        Validators.required,
      ]],
      description: [null, [
        Validators.required,
      ]],
      pages: [null, [
        Validators.required,
      ]],
      secure: [null, [
        Validators.required,
      ]],
      img: [null, [
        Validators.required,
        FileUploadValidators.filesLimit(1)
      ]],
      file: [null, [
        Validators.required,
        FileUploadValidators.filesLimit(1)
      ]]

    })

  }


  close(): void {
    this.dialogRef.close();
  }

  onChanges(): void {
    this.userForm.get('type').valueChanges.subscribe(val => {
      if (this.userForm.controls.type.value == 'audio') {
        this.hideInp = true
        this.userForm.controls.pages.clearValidators()
      }
      else {
        const newValid: ValidatorFn[] = [
          Validators.required,
        ];
        this.userForm.controls.pages.setValidators(newValid)
        this.hideInp = false
      }
      this.userForm.controls.pages.updateValueAndValidity()
    });
  }

  ngOnInit() {
    this.initForm()
    this.onChanges()
    window.addEventListener('resize', () => {
      this.modalHeight()
    })
    window.dispatchEvent(new Event('resize'))
  }

  modalHeight() {
    let elm: HTMLElement = document.querySelector('mat-dialog-container')
    setTimeout(() => {
      if(elm){
        if (window.outerHeight < elm.scrollHeight) {
          elm.classList.add('res')
        }
        else {
          elm.classList.remove('res')
        }
      }
    });
  }

  addMaterial() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
    }
  }


}
