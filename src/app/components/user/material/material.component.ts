import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {

  private tabList = [
    {
      name: "Իմ տեղադրած նյութերը",
      href: "my",
    },
    {
      name: "Իմ պահած նյութերը",
      href: "saved",
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
