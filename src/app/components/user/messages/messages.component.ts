import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  private boolActive: boolean = false;
  private tabList = [
    {
      id: 1,
      name: "Կարեն Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "karen",
      time: "11:25 am"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "gegham",
      time: "22:25 pm"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "hayk",
      time: "15 days"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "saq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "sadq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "sasq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "sdaq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "safq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "saqwe",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "safewfq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գեղամ Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "safwefwefq",
      time: "yesterday"
    },
    {
      id: 2,
      name: "Գե Սարգսյան",
      img: "./assets/img/user.jpg",
      href: "safwefwefq",
      time: "yesterday"
    },
  ]
  private activatedIndex: number;
  public message: {};

  constructor(private messageService: MessageService) {
  }

  findIdx(id: number) {
    this.activatedIndex = id
    this.messageService.messageData(this.tabList[this.activatedIndex]);
    return true;
  }

  ngOnInit() {
  }

}
