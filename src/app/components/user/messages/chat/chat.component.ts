import { Component, OnInit, ViewChild, ElementRef, OnChanges, SimpleChanges, Input } from '@angular/core';
import * as moment from 'moment';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  private unSub: Subscription;
  private message;
  private text: string = '';
  private emoji: boolean = false;
  private theme = {
    martShowHeader: true,
    martShowFooter: false,
    martHeaderPadding: { x: '0px', y: '0px' },
    martFooterPadding: { x: '0px', y: '0px' },
    martHeaderBG: '#e3e7e8',
    martFooterBG: '#e3e7e8',
    martBG: '#ebeff2',
    martCategoryColor: '#94a0a6',
    martCategoryColorActive: '#455a64',
    martActiveCategoryIndicatorColor: '#00897b',
    martEmojiFontSize: '100%',
    martCategoryFontSize: '14px',
    martBorderRadius: '10px',
    martActiveCategoryIndicatorHeight: '4px',
    martEmojiPadding: { x: '40px', y: '40px' }
  }

  constructor(private messageData:MessageService) { 
    this.unSub = this.messageData.message.subscribe(res=>{
      this.message = res

    })

    
  }
  
  
  ngOnInit() {
    if(this.message){
      this.unSub.unsubscribe()
    }
  }

  hideEmoji() {
    this.emoji = false;
  }

  showEmoji(e) {
    this.emoji = !this.emoji;
    window.addEventListener('click', (e) => {
      if (!(e.target as HTMLElement).closest('.emoji') && !(e.target as HTMLElement).closest('ngx-emoj')) {
        this.hideEmoji();
      }
    })
  }

  textareaChange(textarea) {
    this.hideEmoji()
  }

  handleEmoji(e, inp) {
    inp.focus()
    this.text += e.char;
  }

  handleCharDelete(e) {
    if (this.text.length > 0) {
      this.text = this.text.substr(0, this.text.length - 2);
    }
  }

  sendMessage(e) {
    let date = moment().format('LT');
    // console.log(date)
    // console.log(this.text)
  }

}
