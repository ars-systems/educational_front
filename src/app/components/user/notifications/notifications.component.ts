import { Component, OnInit } from '@angular/core';
import { INotification, AppState } from 'src/app/interface/interface';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  // influencer
  private notifications: INotification[] = [
    {
      img: "./assets/img/user.jpg",
      name: "Զավեն Սարգսյան",
      notifications: [
        {
          name: "Ծրագրավորողների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ծրագրավորողների համար",
          img: "./assets/img/group/1.jpg",
          rating: 5,
        },
        {
          name: "Ծրագրավորողների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ծրագրավորողների համար",
          img: "./assets/img/group/1.jpg",
          rating: 5,
        },
        {
          name: "Բժիշկների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է բժիշկների համար",
          img: "./assets/img/group/2.jpg",
          rating: 5,
        },
        {
          name: "Ուսուցիչների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ուսուցիչների համար",
          img: "./assets/img/group/3.jpg",
          rating: 5,
        },
      ]
    },
    {
      img: "./assets/img/user.jpg",
      name: "Զավեն Սարգսյան",
      notifications: [
        {
          name: "Ծրագրավորողների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ծրագրավորողների համար",
          img: "./assets/img/group/1.jpg",
          rating: 5,
        },
        {
          name: "Բժիշկների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է բժիշկների համար",
          img: "./assets/img/group/2.jpg",
          rating: 5,
        },
        {
          name: "Ուսուցիչների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ուսուցիչների համար",
          img: "./assets/img/group/3.jpg",
          rating: 5,
        },
      ]
    },
    {
      img: "./assets/img/user.jpg",
      name: "Զավեն Սարգսյան",
      notifications: [
        {
          name: "Ծրագրավորողների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ծրագրավորողների համար",
          img: "./assets/img/group/1.jpg",
          rating: 5,
        },
        {
          name: "Բժիշկների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է բժիշկների համար",
          img: "./assets/img/group/2.jpg",
          rating: 5,
        },
        {
          name: "Ուսուցիչների խումբ",
          materialMember: 235,
          type: "Թեստեր",
          text: "Խումբը ստեղծված է ուսուցիչների համար",
          img: "./assets/img/group/3.jpg",
          rating: 5,
        },
      ]
    }

  ]

  constructor(private _store: Store<AppState>, private router: Router, private userService: UserService) {

  }
  ngOnInit() {

  }

}
