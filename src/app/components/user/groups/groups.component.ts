import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  private tabList = [
    {
      name: "Իմ ստեղծած խմբերը",
      href: "my",
    },
    {
      name: "Իմ նախընտրած խմբերը",
      href: "saved",
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
