import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddGroupComponent } from '../add-group/add-group.component';

@Component({
  selector: 'app-my-groups',
  templateUrl: './my-groups.component.html',
  styleUrls: ['./my-groups.component.scss']
})
export class MyGroupsComponent implements OnInit {

  private p: number = 1;
  private inputVal: string = '';
  private myGroups = [
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 5,
    },
    {
      name: "Ուսուցիչների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ուսուցիչների համար",
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "Ծրագրավորողների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է ծրագրավորողների համար",
      img: "./assets/img/group/1.jpg",
      rating: 5,
    },
    {
      name: "Բժիշկների խումբ",
      groupMember: 235,
      text: "Խումբը ստեղծված է բժիշկների համար",
      img: "./assets/img/group/2.jpg",
      rating: 4.5,
    },

  ]

  private newMaterial: string;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddGroupComponent, {
      width: '550px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newMaterial = result;
    });
  }

}
