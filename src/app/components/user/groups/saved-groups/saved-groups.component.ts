import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saved-groups',
  templateUrl: './saved-groups.component.html',
  styleUrls: ['./saved-groups.component.scss','../my-groups/my-groups.component.scss']
})
export class SavedGroupsComponent implements OnInit {

  private inputVal:string = '';
  private savedGroups = [
    {
      name: "ՀՊՏՀ ուսանողների խումբ",
      author: "Հեղինե Մանուկյան",
      groupMember: 35,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "ՀՊՏՀ ուսանողների խումբ",
      author: "Հեղինե Մանուկյան",
      groupMember: 35,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
    {
      name: "ՀՊՏՀ ուսանողների խումբ",
      author: "Հեղինե Մանուկյան",
      groupMember: 35,
      img: "./assets/img/group/3.jpg",
      rating: 5,
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
