import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { ValidatorFn, FileUploadValidators } from '@iplab/ngx-file-upload';
import { MatDialogRef } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {

  private hideInp: boolean = false;
  private userForm: FormGroup;
  private secureVal = '1';
  private visible = true;
  private selectable = true;
  private removable = true;
  private addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  private tags = [];
  private category: {}[] = [
    { label: "Քիմիա", value: "qimia" },
    { label: "Սոցիոլոգիա", value: "soliologia" },
    { label: "Հոգեբանություն", value: "hogebanutyun" },
    { label: "Քաղաքագիտություն", value: "qaxaqagitutyun" },
    { label: "Լեզուներ", value: "lezuner" },
    { label: "Պատմություն", value: "patmutyun" },
    { label: "Իրավաբանություն", value: "iravabanutyun" },
    { label: "Աշխարհագրություն", value: "ashxarhagrutyun" },
    { label: "Մշակույթ", value: "mshakuyt" },
    { label: "Ինֆորմատիկա", value: "informatika" },
    { label: "Տնտեսագիտություն", value: "tntesagitutyun" },
    { label: "Մաթեմատիկա", value: "matematika" },
    { label: "Ֆիզիկա", value: "fizika" },
  ]

  private users = [
    {
      img: "./assets/img/user.jpg",
      name: "Zaven",
      surname: "Sargsyan",
    },
    {
      img: "./assets/img/user.jpg",
      name: "Karen",
      surname: "Mnacakanyan",
    },
    {
      img: "./assets/img/user.jpg",
      name: "Zaven",
      surname: "Sargsyan",
    },
    {
      img: "./assets/img/user.jpg",
      name: "Zaven",
      surname: "Sargsyan",
    },
    {
      img: "./assets/img/user.jpg",
      name: "Zaven",
      surname: "Sargsyan",
    },
  ]

  constructor(public dialogRef: MatDialogRef<AddGroupComponent>, private fb: FormBuilder) { }

  private initForm(): void {
    this.userForm = this.fb.group({
      img: [null, [
        Validators.required,
        FileUploadValidators.filesLimit(1)
      ]],
      groupName: [null, [
        Validators.required,
      ]],
      description: [null, [
        Validators.required,
      ]],
      secure: [null, [
        Validators.required,
      ]],
      category: [null, [
        Validators.required,
      ]],
      tags: [[], [
      ]]

    })
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.initForm()
    window.addEventListener('resize', () => {
      this.modalHeight()
    })
    window.dispatchEvent(new Event('resize'))
  }

  modalHeight() {
    let elm: HTMLElement = document.querySelector('mat-dialog-container')
    setTimeout(() => {
      if(elm){
        if (window.outerHeight < elm.scrollHeight) {
          elm.classList.add('res')
        }
        else {
          elm.classList.remove('res')
        }
      }
    });
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.tags.push({ name: value.trim() });
    }
    if (input) {
      input.value = '';
    }
    this.updateFormControlTag(this.tags)
  }

  updateFormControlTag(tags: { name: string }[]) {
    const control = <FormArray>this.userForm.controls['tags'];
    const arr = [];
    tags.forEach(tag => {
      arr.push(tag.name)
    });
    control.setValue(arr)
  }

  removeTag(tag): void {
    const index = this.tags.indexOf(tag);
    if (index >= 0) {
      this.tags.splice(index, 1);
      this.updateFormControlTag(this.tags)
    }
  }

  addGroup() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
    }
  }
}
