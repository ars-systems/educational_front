import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-participated-live',
  templateUrl: './participated-live.component.html',
  styleUrls: ['./participated-live.component.scss','../upcoming-live/upcoming-live.component.scss']
})
export class ParticipatedLiveComponent implements OnInit {

  private participatedCourses = [
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
