import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  private tabList = [
    {
      name: "Իմ ստեղծած",
      href: "my",
    },
    {
      name: "Առաջիկա",
      href: "upcoming",
    },
    {
      name: "Մասնակցած",
      href: "participated",
    },
  ]

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  private newMaterial:string;




}
