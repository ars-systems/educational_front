import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddLiveComponent } from '../add-live/add-live.component';

@Component({
  selector: 'app-my-live',
  templateUrl: './my-live.component.html',
  styleUrls: ['./my-live.component.scss','../upcoming-live/upcoming-live.component.scss']
})
export class MyLiveComponent implements OnInit {

  private myLiveCourses = [
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
  ]

  constructor( private dialog:MatDialog) { }

  ngOnInit() {
  }

  private newMaterial:string;

  openDialog(): void {
    const dialogRef = this.dialog.open(AddLiveComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newMaterial = result;
    });
  }

}
