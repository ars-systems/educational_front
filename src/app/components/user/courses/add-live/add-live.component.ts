import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, ValidatorFn, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { FileUploadValidators } from '@iplab/ngx-file-upload';

@Component({
  selector: 'app-add-live',
  templateUrl: './add-live.component.html',
  styleUrls: ['./add-live.component.scss']
})
export class AddLiveComponent implements OnInit {

  private hideInp: boolean = false;
  private userForm: FormGroup;
  private secureVal = '1';

  constructor(public dialogRef: MatDialogRef<AddLiveComponent>, private fb: FormBuilder) { }

  private initForm(): void {
    this.userForm = this.fb.group({
      name: [null, [
        Validators.required,
      ]],
      topic: [null, [
        Validators.required,
      ]],
      description: [null, [
        Validators.required,
      ]],
      maxCount: [null, [
        Validators.required,
      ]],
      date: [null, [
        Validators.required,
      ]],
    })
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.initForm()
    window.addEventListener('resize', () => {
      this.modalHeight()
    })
    window.dispatchEvent(new Event('resize'))
  }

  modalHeight() {
    let elm: HTMLElement = document.querySelector('mat-dialog-container')
    setTimeout(() => {
      if(elm){
        if (window.outerHeight < elm.scrollHeight) {
          elm.classList.add('res')
        }
        else {
          elm.classList.remove('res')
        }
      }
    });
  }

  addLive() {
    const controls = this.userForm.controls
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName => {
        return controls[controlName].markAsTouched()
      });
      return;
    }
    else {
    }
  }

}
