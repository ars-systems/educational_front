import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upcoming-live',
  templateUrl: './upcoming-live.component.html',
  styleUrls: ['./upcoming-live.component.scss']
})
export class UpcomingLiveComponent implements OnInit {

  private upcomingCourses = [
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
    {
      name: "Ինչպես դառնալ ծրագրավորող։ Դաս 1",
      author: "Varazdat Shirxanyan",
      img: "./assets/img/group/1.jpg",
      rating: 5,
      day: "22.01.12",
      time: "22:00"
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
