import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { IDiagram } from 'src/app/interface/interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('diagram') diagram: ElementRef;
  public checkDiagram: boolean = false;
  private canvasData: IDiagram[] = [
    {
      name: "Կուրսայիններ",
      percent: 45,
      color: "red",
    },
    {
      name: "Դիպլոմայիններ",
      percent: 55,
      color: 'orange',
    },
    {
      name: "Թեստեր",
      percent: 65,
      color: "blue",
    },
    {
      name: "Աուդիո գրքեր",
      percent: 75,
      color: "green"
    },
    {
      name: "Գրքեր",
      percent: 85,
    },

  ]
  private user;

  constructor(private userService: UserService) { 
  }

  ngOnInit() {
    window.addEventListener('scroll', () => {
      if (document.documentElement.scrollTop + document.documentElement.offsetHeight >= this.diagram.nativeElement.offsetTop) {
        this.checkDiagram = true;
      }
    });
  }

  ngAfterViewInit() {
  }

}