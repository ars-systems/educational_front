import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, Renderer2, AfterViewInit, ɵConsole, Renderer } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as Actions from '../../store/actions/methods.actions'
import Swiper from 'swiper'
import { first } from 'rxjs/operators';
import { INav, AppState } from 'src/app/interface/interface';
import { UrlsService } from 'src/app/services/urls.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, AfterViewInit {

  private loginModal: boolean = false;
  private toggleMenu: boolean = false;
  private toggleDropDown: boolean = false;
  private isLoggedIn: boolean = false;
  private user;

  @ViewChildren('container') container: QueryList<ElementRef>;
  @ViewChild('slideContent') slideContent: ElementRef;
  @ViewChild('slide') slide: ElementRef;
  @ViewChild('menu') menu: ElementRef;
  // @ViewChild('next') next: ElementRef;
  // @ViewChild('prev') prev: ElementRef;

  private navData: INav[] = [
    {
      name: "Գրքեր",
      icon: "home",
      href: "/user",
      subMenu: [
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
      ]
    },
    {
      name: "Աուդիո Գրքեր",
      icon: "home",
      subMenu: [
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
      ]
    },
    {
      name: "Օնլայն դասեր",
      icon: "home",
      subMenu: [
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
      ]

    },
    {
      name: "Թեստեր",
      icon: "home",
      subMenu: [
        {
          name: "Գրքեր",
          icon: "home",
        },
        {
          name: "Գրքեր",
          icon: "home",
        },
      ]
    },
    {
      name: "Օգտակար Նյութեր",
      icon: "home",
      subMenu: [
        {
          name: "Հոդվածներ",
          icon: "home",
        },
        {
          name: "Ռեֆերատներ",
          icon: "home",
        },
        {
          name: "Կուրսային աշխատանքներ",
          icon: "home",
        },
        {
          name: "Դիպլոմային աշխատանքներ",
          icon: "home",
        },
        {
          name: "Մագիստրոսական թեզեր",
          icon: "home",
        },
      ]
    }
  ]

  constructor(private _store: Store<AppState>, private renderer: Renderer2, private urls: UrlsService) {
    this._store.pipe(select('_methods')).subscribe(res => {
      // console.log(res)
      this.user = res.user;
      this.isLoggedIn = res.user ? true : false;
    })
  }

  private uploadUrl: string = this.urls.uploadUrl;


  dropDown(event) {
    this.toggleMenu = false;
    var submenu: HTMLDivElement = event.srcElement.parentElement;
    var bool = submenu.classList.contains('show')
    if (document.querySelector('.show')) {
      document.querySelector('.show').classList.remove('show')
    }
    if (!bool) {
      submenu.classList.add('show')
    }

    document.querySelector('nav').addEventListener('mouseleave', () => {
      setTimeout(() => {
        if (document.querySelector('.show'))
          document.querySelector('.show').classList.remove('show')
      }, 50);
    })
  }

  resize() {
    if (window.innerWidth <= 1100) {
      this.toggleDropDown = true;
      this.menu.nativeElement.classList.add('toggle')
    }
    else {
      this.toggleMenu = false;
      this.toggleDropDown = false;
      this.menu.nativeElement.classList.remove('toggle')
    }
  }

  ngOnInit() {
    window.addEventListener('resize', () => {
      this.resize()
    })
    window.dispatchEvent(new Event('resize'))

    document.querySelector('body').addEventListener('click', (e) => {
      if (!(event.target as HTMLElement).closest(".menu") && !(event.target as HTMLElement).closest("#toggle")) {
        this.toggleMenu = false;
      }
      if ((event.target as HTMLElement).closest(".toggle .dropdown-menu")) {
        this.toggleMenu = false;
      }
    })
  }

  ngAfterViewInit() {
    this.navSlider()
  }
  count = 0;

  navSlider() {
    let first;
    let last;

    this.container.forEach((element, id) => {
      let options: {} = {};

      if (this.navData[id].subMenu.length > 6) {
        options = {
          slidesPerView: 6,
          speed: 2000,
          watchSlidesVisibility: true,
        }

        element.nativeElement.addEventListener('mousemove', () => {
          first = element.nativeElement.querySelector('.swiper-slide-active')
          last = element.nativeElement.querySelectorAll('.swiper-slide-visible')[5]

          first.addEventListener('mousemove', (e: MouseEvent) => {
            e.preventDefault();
            swiper.slidePrev();
          })
          last.addEventListener('mousemove', (e: MouseEvent) => {
            e.preventDefault();
            swiper.slideNext();
          })
        })

      }
      else {
        (element.nativeElement as HTMLDivElement).classList.add('center')
        options = {
          slidesPerView: 6,
          allowSlidePrev: false,
          allowSlideNext: false,
        }
      }

      let swiper = new Swiper(element.nativeElement, options);

    });
  }

  updateState(val) {
    this._store.dispatch(new Actions.LoginModal(true))
    this._store.dispatch(new Actions.LoginorRegistration(val))
  }
}