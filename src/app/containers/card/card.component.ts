import { Component, OnInit, Input } from '@angular/core';
import { ICardItem } from 'src/app/interface/interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() item:ICardItem;

  constructor() { }
  
  ngOnInit() {
  }

}
