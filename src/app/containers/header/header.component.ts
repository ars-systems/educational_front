import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as Actions from '../../store/actions/methods.actions'
import { AppState } from 'src/app/interface/interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private isLoggedIn:boolean = false;

  constructor(private _store: Store<AppState>) { 
    let token = localStorage.getItem('user')
    if(token){
      this.isLoggedIn = true
    }
    else{
      this.isLoggedIn = false
    }
  }
  ngOnInit() {
  }

  updateState(val){
    this._store.dispatch(new Actions.LoginModal(true))
    this._store.dispatch(new Actions.LoginorRegistration(val))
  }

}
