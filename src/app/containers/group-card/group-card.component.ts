import { Component, OnInit, Input } from '@angular/core';
import { ICardItem } from 'src/app/interface/interface';

@Component({
  selector: 'app-group-card',
  templateUrl: './group-card.component.html',
  styleUrls: ['./group-card.component.scss']
})
export class GroupCardComponent implements OnInit {

  @Input() item:ICardItem;

  constructor() { }

  ngOnInit() {
  }

}
