import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
    providedIn: 'root'
})
export class UrlsService {

    configUrl:string = environment.baseUrl
    uploadUrl:string = this.configUrl + '/static/';

    constructor() {
    }


}
