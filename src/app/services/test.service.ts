import { Injectable } from '@angular/core';
import { element } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  haveErr:boolean = false;

  constructor() { }

  addingTest(testData) {
    testData.forEach(elm => {
      this.singleTest(elm)
    })
  }

  singleTest(elm) {
    delete elm.question.error
    elm.question.error = {}
    var checkedAnswer: boolean = false;
    if (elm.question.options) {
      let writedAnswers = [];
      elm.question.options.forEach(element => {
        if (element.checked == true) {
          checkedAnswer = true
        }
        if (String(element.label).trim().length === 0) {
          elm.question.error.emptyLinesError = "Լրացրեք դատարկ դաշտերը";
        }
        else {
          writedAnswers.push(element.label)
        }
      });
      if (writedAnswers.length < 2) {
        elm.question.error.lessLinesError = "Հարցը պետք է ունենա առնվազն 2 պատասխան";
      }

    }
    if (elm.question.type == 0) {
      if (elm.question.pic == null) {
        elm.question.error.picError = "Նկարը պարտադիր է"
      }
      if (!checkedAnswer) {
        elm.question.error.answerOptionError = "Ընտրեք ճիշտ տարբերակը (կամ տարբերակները)"
      }
    }
    else if (elm.question.type == 1) {
      if (!checkedAnswer) {
        elm.question.error.answerOptionError = "Ընտրեք ճիշտ տարբերակը (կամ տարբերակները)"
      }
    }
    else {
      if (String(elm.question.answer).trim().length < 1) {
        elm.question.error.answerError = "Պատասխանը պարտադիր է"
      }
    }
    if (String(elm.question.ask).trim().length < 1) {
      elm.question.error.askError = "Հարցը պարտադիր է"
    }

    if((Object.keys(elm.question.error).length === 0)){
      return this.haveErr = false
    }
    else{
      return this.haveErr = true
    }


  }

}
