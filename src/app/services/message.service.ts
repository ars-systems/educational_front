import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MessageService {
  message: Observable<any>;
  private messageSubject = new Subject<any>();

  constructor() {
    this.message = this.messageSubject.asObservable();
  }

  messageData(data) {
    this.messageSubject.next(data);
  }


}