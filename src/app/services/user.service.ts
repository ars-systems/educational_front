import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState } from '../interface/interface';
import { FormGroup } from '@angular/forms';
import { of } from 'rxjs';
import { UrlsService } from './urls.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token: string;

  constructor(public http: HttpClient, private _store: Store<AppState>, private urls: UrlsService) {
    this._store.pipe(select('_methods')).subscribe(res => {
      this.token = res.userToken
    })
  }

  private configUrl = this.urls.configUrl


  addUser(user: Object) {
    const date = new Date()
    const data = {
      name: user['name'],
      email: (user['email'] as string).toLowerCase(),
      password: user['password'],
      role: user['role'],
      gender: user['gender'],
      created_date: date
    }
    return this.http
      .post(this.configUrl + '/users', data)
      .pipe(map(res => res))
  }

  login(user: Object) {
    const data = {
      email: (user['email'] as string).toLowerCase(),
      password: user['password']
    }
    return this.http
      .post(this.configUrl + "/login", data)
      .pipe(
        map(res => res),
      )
  }

  sendToken() {
    const data = {
      token: this.token,
    }
    return this.http
      .post(this.configUrl + '/getUsersWithToken', data)
      .pipe(map(res => res))
  }

  editUser(updateData: FormGroup, img:string) {
    const date = new Date();
    const formData = new FormData();
    formData.append('token', this.token);
    formData.append('updated_date', JSON.stringify(date));
    formData.append('name', updateData.get('name').value);
    formData.append('surname', updateData.get('surname').value);
    formData.append('newPassword', updateData.get('newPassword').value);
    formData.append('password', updateData.get('password').value);
    formData.append('role', updateData.get('role').value);
    formData.append('gender', updateData.get('gender').value);
    formData.append('profession', updateData.get('profession').value);
    formData.append('university', updateData.get('university').value);
    formData.append('blockMassage', updateData.get('blockMassage').value);
    formData.append('work', updateData.get('work').value);
    if(updateData.get('role').value == 'other'){
      formData.append('otherName', updateData.get('otherName').value);
    }
    if(updateData.get('image').value[0].file){
      formData.append('image', updateData.get('image').value[0].file);
    }
    else{
      formData.append('image', img);
    }

    return this.http
      .post(this.configUrl + "/user/update?=" + this.token, formData)
      .pipe(
        map(res => res)
      )
  }


  allUsers() {
    return this.http
      .get(this.configUrl + "/users")
      .pipe(
        map(res => res),
      )
  }


  allUsersNotFriend(p:number) {
    const data = {
      token: this.token,
      pageNumber: p
    }
    return this.http
      .post(this.configUrl + "/not_friend", data)
      .pipe(
        map(res => res),
      )
  }

  getFriend(p: number) {
    const data = {
      token: this.token,
      pageNumber: p
    }
    return this.http
      .post(this.configUrl + "/get_friends", data)
      .pipe(
        map(res => res),
      )
  }

  removeFriend(userId:number) {
    const data = {
      token: this.token,
      friend_id: userId
    }
    return this.http
      .post(this.configUrl + "/rm_friend", data)
      .pipe(
        map(res => res),
      )
  }

  addPreferableUser(userId: number) {
    const data = {
      token: this.token,
      friend_id: userId
    }
    return this.http
      .post(this.configUrl + "/send_friend_id", data)
      .pipe(
        map(res => res),
      )
  }

}
