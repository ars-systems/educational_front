import { Injectable } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class EmailValidateService {

  private users: string[] = []
  private usersList:Subscription;

  constructor(private http: HttpClient, private userService: UserService) { 
    if(this.usersList){
      this.usersList.unsubscribe()
    }
  }

  validateName(userName: string): Observable<ValidationErrors> {
    return new Observable<ValidationErrors>(obs=>{
      this.usersList = this.userService
      .allUsers()
      .subscribe(data => {
        data['data'].forEach(element => {
          this.users.push(element.email)
        });
      })
      
      const user = this.users.find(user=> user === userName)
      
      if(user){
        obs.next({
          invalid: "Մեյլը զբաղված է !"
        });
        obs.complete()
      }

      obs.next(null)
      obs.complete()
    }).pipe(
      delay(1000)
    )
  }

}
