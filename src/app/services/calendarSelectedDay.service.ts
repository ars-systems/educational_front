import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CalendarSelectedDayService {
  day: Observable<any>;
  private calendarSubject = new Subject<any>();

  constructor() {
    this.day = this.calendarSubject.asObservable();
  }

  calendarSelectedDay(data) {
    this.calendarSubject.next(data);
  }


}